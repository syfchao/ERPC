
正如前面文档所介绍的，整个ERPC底层通信协议采用标准的json-rpc通信格式框架。有关协议规则详见[json-rpc](../Other/1.json-rpc.md)。

# 1. 整体框架

每个ERPC的通信消息报文，是由jsonrpc、id、direction、sender、receiver、module、method、params（或result，或error）八个字段组成：
- jsonrpc：固定为2.0，代表使用的jsonrpc标准2.0版本协议；
- id：为每次包的帧号，由主动发起者变更（请求和通知报文），接收者保持不变进行返回（响应和错误返回）；
- direction代表消息方向，也指报文类型，可取值为：
	- Request ：是请求报文，需要服务进行响应；
	- Response ：是请求报文的响应报文，需保持id与请求报文一致；
	- Notice ：是通知报文，不需要响应报文；同一通知报文的多次广播id保持不变，且Notice报文的receiver域不存在；
- sender：当前报文发送进程的名字；
- receiver：当前报文接收进程的名字；
- module：此次报文所属模块名称；
- method：此次报文对应的服务名称；
- params（或result，或error）：此次报文的相关参数：若为请求报文，则为params；若为响应报文，则为result；若为错误返回，则为error。

协议其他说明：
1. 请求报文（Request）和通知报文（Notice）的params可以有也可以没有，也即可不带参数；
2. 服务端（service）函数执行完后，可以返回为NULL，也代表执行成功，对应的远程调用不需要对返回参数做处理，只需要关注rpc_service_proxy_call()返回0即代表执行成功；
3. module为ERPCFramework时，代表ERPC框架内部模块，对应的method为ERPC框架内部函数方法，可取值为：
	- ERPCLinkRequest ：为链接请求包，无params，只需要判断sender和receiver即可；
	- ERPCRegisterObserver ：为注册观察者请求包，params要求带有module和observer参数；
	- ERPCUnregisterObserver ：为注销观察者请求包，params要求带有module和observer参数；
4. module不为ERPCFramework时，代表真正业务的模块，与method一起确定请求的服务函数；
5. 注意无论是module还是method前缀都不要使用ERPC（或erpc），以ERPC（或erpc）为前缀的协议预留用作ERPC框架使用。

# 2. 应用举例

客户端发起请求报文：
```
{
    "jsonrpc": "2.0",
    "id": "122455455",
    "direction": "Request",
    "sender": "APPService",
    "receiver": "MediaService",
    "module": "ERPCFramework",
    "method": "ERPCRegisterObserver",
    "params": {
		"module": "Media",
		"observer": "State",
        }
    }
}
```

服务端注册成功返回：
```
{
    "jsonrpc": "2.0",
    "id": "122455455",
    "direction": "Response",
    "sender": "MediaService",
    "receiver": "APPService",
    "module": "ERPCFramework",
    "method": "ERPCRegisterObserver",
    "result": "Success"
}
```


服务端注册失败返回：
```
{
	"jsonrpc": "2.0",
    "id": "122455455",
    "direction": "Response",
    "sender": "MediaService",
    "receiver": "APPService",
    "module": "ERPCFramework",
    "method": "ERPCRegisterObserver",
    "error": {
        "code": -32601,
        "message": "observer not found!",
    }
}
```
